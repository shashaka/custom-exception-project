package org.blog.test.service.impl;

import org.blog.test.common.CustomException;
import org.blog.test.service.TestService;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {

    @Override
    public void test() {
        throw new CustomException("custom exception occured");
    }
}
